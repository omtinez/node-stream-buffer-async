import { WritableStreamBuffer } from './writable';

export interface StreamLike {
    once(event: string, listener: () => void): this;
    once(event: 'error', listener: (err: Error) => void): this;
}

export interface IReadableStream extends StreamLike {
    pipe<T extends NodeJS.WritableStream>(destination: T, options?: { end?: boolean; }): T;
    unpipe<T extends NodeJS.WritableStream>(destination?: T): this;
}
function isReadable(stream: any): stream is IReadableStream {
    return stream.pipe !== undefined && stream.unpipe !== undefined;
}

export interface IWritableStream extends StreamLike {
    write(chunk: any, encoding?: string, cb?: (error: Error | null | undefined) => void): boolean;
}
function isWritable(stream: any): stream is IWritableStream {
    return stream.write !== undefined;
}

export interface IEndableStream extends StreamLike {
    end(chunk: any, encoding?: string, cb?: () => void): void;
}
function isEndable(stream: any): stream is IEndableStream {
    return stream.end !== undefined;
}

export interface IAsynchronousStream {
    endAsync(chunk?: any, encoding?: string): Promise<void>;
    writeAsync(chunk: any, encoding?: string): Promise<boolean>;
    readAsync(event?: string): Promise<Buffer>;
}

export function StreamAsync<T extends StreamLike>(stream: T): T & IAsynchronousStream {
    const stream_ = stream as T & IAsynchronousStream;

    if (isReadable(stream_)) {
        stream_.readAsync = (event: string = 'end') => {
            return new Promise<Buffer>((resolve, reject) => {

                // Setup error handling
                stream_.once('error', reject);

                // Pipe output into buffer
                const buffer = new WritableStreamBuffer();
                stream_.pipe(buffer);

                // Terminate with output end
                stream_.once(event, () => {
                    stream_.unpipe(buffer);
                    resolve(buffer.getContents() || Buffer.from(''));
                });
            });
        }
    }

    if (isWritable(stream_)) {
        stream_.writeAsync = (chunk: any, encoding: string = 'UTF8') => {
            return new Promise<boolean>((resolve, reject) => {
                stream_.once('error', reject);
                const result = stream_.write(chunk, encoding, err => err && reject(err));
                result ? resolve() : reject(new Error('Write operation failed'));
            });
        };
    }

    if (isEndable(stream_)) {
        stream_.endAsync = (chunk?: any, encoding: string = 'UTF8') => {
            return new Promise<void>((resolve, reject) => {
                stream_.once('error', reject);
                stream_.end(chunk, encoding, resolve);
            });
        };
    }

    return stream_;
}