import * as constants from '../constants';

export const simpleString = 'This is a String!';
export const unicodeString = '\u00bd + \u00bc = \u00be';
export const binaryData = new Buffer(64);
for(var i = 0; i < binaryData.length; i++) {
  binaryData[i] = i;
}

// Binary data larger than initial size of buffers.
export const largeBinaryData = Buffer.alloc(constants.DEFAULT_INITIAL_SIZE + 1);
for(i = 0; i < largeBinaryData.length; i++) {
  largeBinaryData[i] = i % 256;
}