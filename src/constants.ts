export const DEFAULT_INITIAL_SIZE = (8 * 1024);
export const DEFAULT_INCREMENT_AMOUNT = (8 * 1024);
export const DEFAULT_FREQUENCY = 1;
export const DEFAULT_CHUNK_SIZE = 1024;
export const DEFAULT_ENCODING = 'UTF8';