import * as stream from 'stream';
import * as constants from './constants';

export interface WritableStreamBufferOptions extends stream.WritableOptions {
    initialSize?: number;
    incrementAmount?: number;
}

const _defaultOptions = {
    frequency: constants.DEFAULT_FREQUENCY,
    chunkSize: constants.DEFAULT_CHUNK_SIZE,
    initialSize: constants.DEFAULT_INITIAL_SIZE,
    incrementAmount: constants.DEFAULT_INCREMENT_AMOUNT
} as WritableStreamBufferOptions;

export class WritableStreamBuffer extends stream.Writable {
    private size_ = 0;

    private initialSize: number;
    private incrementAmount: number;

    private buffer: Buffer;

    constructor(opts?: WritableStreamBufferOptions) {
        super(Object.assign({}, _defaultOptions, opts));
        Object.assign(this, _defaultOptions, opts);
        this.buffer = Buffer.alloc(this.initialSize);
    }

    size() {
        return this.size_;
    }

    maxSize() {
        return this.buffer.length;
    }

    private increaseBufferIfNecessary(incomingDataSize: number) {
        if((this.buffer.length - this.size_) < incomingDataSize) {
            var factor = Math.ceil(
                (incomingDataSize - (this.buffer.length - this.size_)) / this.incrementAmount);

            var newBuffer = Buffer.alloc(this.buffer.length + (this.incrementAmount * factor));
            this.buffer.copy(newBuffer, 0, 0, this.size_);
            this.buffer = newBuffer;
        }
    }

    getContents(length?: number) {
        if(!this.size_) return Buffer.from('');

        const data = Buffer.alloc(Math.min(length || this.size_, this.size_));
        this.buffer.copy(data, 0, 0, data.length);

        if(data.length < this.size_)
            this.buffer.copy(this.buffer, 0, data.length);

        this.size_ -= data.length;
        return data;
    }

    getContentsAsString(encoding: string = constants.DEFAULT_ENCODING, length?: number) {
        if(!this.size_) return '';

        var data = this.buffer.toString(encoding, 0, Math.min(length || this.size_, this.size_));
        var dataLength = Buffer.byteLength(data);

        if(dataLength < this.size_)
        this.buffer.copy(this.buffer, 0, dataLength);

        this.size_ -= dataLength;
        return data;
    }

    _write(chunk: any, encoding = constants.DEFAULT_ENCODING, callback: (error?: Error) => void) {
        this.increaseBufferIfNecessary(chunk.length);
        chunk.copy(this.buffer, this.size_, 0);
        this.size_ += chunk.length;
        callback();
    }
};