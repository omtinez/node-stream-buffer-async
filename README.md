# Node Stream Buffers Async

Implementation of Readable and Writable Streams that use a [Buffer][node-buffer-docs] and [Promises][node-promise-docs] in [typescript][typescript-url]. Forked from [Node Stream Buffer][node-stream-buffer].

```
yarn add https://gitlab.com/omtinez/stream-buffers-async
```

## Usage

To use the stream buffers in your module, simply import it and away you go.

```ts
import { ReadableStreamBuffer, WriteableStreamBuffer } from 'stream-buffers-async';
```

### WritableStreamBuffer

`WritableStreamBuffer` implements the standard [`stream.Writable`](https://nodejs.org/api/stream.html#stream_class_stream_writable) interface. All writes to this stream will accumulate in an internal [`Buffer`](https://nodejs.org/api/buffer.html). If the internal buffer overflows it will be resized automatically. The initial size of the Buffer and the amount in which it grows can be configured in the constructor.

```ts
const myWritableStreamBuffer = new WritableStreamBuffer({
	initialSize: (100 * 1024),   // start at 100 kilobytes.
	incrementAmount: (10 * 1024) // grow by 10 kilobytes each time buffer overflows.
});
```

The default initial size and increment amount are stored in the following constants:

```ts
DEFAULT_INITIAL_SIZE      // (8 * 1024)
DEFAULT_INCREMENT_AMOUNT  // (8 * 1024)
```

Writing is standard Stream stuff:

```ts
await myWritableStreamBuffer.write(myBuffer);
// - or -
await myWritableStreamBuffer.write('\u00bd + \u00bc = \u00be', 'UTF8');
```

You can query the size of the data being held in the Buffer, and also how big the Buffer's max capacity currently is:

```ts
await myWritableStreamBuffer.write('ASDF');
streamBuffers.size();     // 4.
streamBuffers.maxSize();  // Whatever was configured as initial size. In our example: (100 * 1024).
```

## TODO

Document `StreamAsync` stream wrapper.

## Contributors

Thanks to the following people for taking some time to contribute to this project.

 * Igor Dralyuk <idralyuk@ebay.com>
 * Simon Koudijs <simon.koudijs@intellifi.nl>

## License

node-stream-buffer-async is free and unencumbered public domain software. For more information, see the accompanying UNLICENSE file.

[typescript-url]: https://www.typescriptlang.org
[node-buffer-docs]: http://nodejs.org/api/buffer.html
[node-stream-buffer]: https://github.com/samcday/node-stream-buffer