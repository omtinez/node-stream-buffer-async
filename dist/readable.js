"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const stream = require("stream");
const constants = require("./constants");
const _defaultOptions = {
    frequency: constants.DEFAULT_FREQUENCY,
    chunkSize: constants.DEFAULT_CHUNK_SIZE,
    initialSize: constants.DEFAULT_INITIAL_SIZE,
    incrementAmount: constants.DEFAULT_INCREMENT_AMOUNT
};
class ReadableStreamBuffer extends stream.Readable {
    constructor(opts) {
        super(opts);
        this.size_ = 0;
        this.stopped = false;
        this.allowPush = false;
        Object.assign(this, _defaultOptions, opts);
        this.buffer = Buffer.alloc(this.initialSize);
        // StreamAsync(this);
    }
    size() {
        return this.size_;
    }
    maxSize() {
        return this.buffer.length;
    }
    increaseBufferIfNecessary(incomingDataSize) {
        if ((this.buffer.length - this.size_) < incomingDataSize) {
            var factor = Math.ceil((incomingDataSize - (this.buffer.length - this.size_)) / this.incrementAmount);
            var newBuffer = Buffer.alloc(this.buffer.length + (this.incrementAmount * factor));
            this.buffer.copy(newBuffer, 0, 0, this.size_);
            this.buffer = newBuffer;
        }
    }
    sendData() {
        const amount = Math.min(this.chunkSize, this.size_);
        let sendMore = false;
        if (amount > 0) {
            const chunk = Buffer.alloc(amount);
            this.buffer.copy(chunk, 0, 0, amount);
            sendMore = this.push(chunk) !== false;
            this.allowPush = sendMore;
            this.buffer.copy(this.buffer, 0, amount, this.size_);
            this.size_ -= amount;
        }
        if (this.size_ === 0 && this.stopped) {
            this.push(null);
        }
        if (sendMore) {
            this.timeout = setTimeout(() => this.sendData(), this.frequency);
        }
        else {
            this.timeout = null;
        }
    }
    stop() {
        if (this.stopped) {
            throw new Error('stop() called on already stopped ReadableStreamBuffer');
        }
        this.stopped = true;
        if (this.size_ === 0) {
            this.push(null);
        }
    }
    kickSendDataTask() {
        if (!this.timeout && this.allowPush) {
            this.timeout = setTimeout(() => this.sendData(), this.frequency);
        }
    }
    put(data, encoding = constants.DEFAULT_ENCODING) {
        if (this.stopped) {
            throw new Error('Tried to write data to a stopped ReadableStreamBuffer');
        }
        if (Buffer.isBuffer(data)) {
            this.increaseBufferIfNecessary(data.length);
            data.copy(this.buffer, this.size_, 0);
            this.size_ += data.length;
        }
        else {
            data = data + '';
            const dataSizeInBytes = Buffer.byteLength(data);
            this.increaseBufferIfNecessary(dataSizeInBytes);
            this.buffer.write(data, this.size_, dataSizeInBytes, encoding);
            this.size_ += dataSizeInBytes;
        }
        this.kickSendDataTask();
    }
    _read() {
        this.allowPush = true;
        this.kickSendDataTask();
    }
}
exports.ReadableStreamBuffer = ReadableStreamBuffer;
