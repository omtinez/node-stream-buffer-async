/// <reference types="node" />
export interface StreamLike {
    once(event: string, listener: () => void): this;
    once(event: 'error', listener: (err: Error) => void): this;
}
export interface IReadableStream extends StreamLike {
    pipe<T extends NodeJS.WritableStream>(destination: T, options?: {
        end?: boolean;
    }): T;
    unpipe<T extends NodeJS.WritableStream>(destination?: T): this;
}
export interface IWritableStream extends StreamLike {
    write(chunk: any, encoding?: string, cb?: (error: Error | null | undefined) => void): boolean;
}
export interface IEndableStream extends StreamLike {
    end(chunk: any, encoding?: string, cb?: () => void): void;
}
export interface IAsynchronousStream {
    endAsync(chunk?: any, encoding?: string): Promise<void>;
    writeAsync(chunk: any, encoding?: string): Promise<boolean>;
    readAsync(event?: string): Promise<Buffer>;
}
export declare function StreamAsync<T extends StreamLike>(stream: T): T & IAsynchronousStream;
//# sourceMappingURL=promisify.d.ts.map