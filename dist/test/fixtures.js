"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants = require("../constants");
exports.simpleString = 'This is a String!';
exports.unicodeString = '\u00bd + \u00bc = \u00be';
exports.binaryData = new Buffer(64);
for (var i = 0; i < exports.binaryData.length; i++) {
    exports.binaryData[i] = i;
}
// Binary data larger than initial size of buffers.
exports.largeBinaryData = Buffer.alloc(constants.DEFAULT_INITIAL_SIZE + 1);
for (i = 0; i < exports.largeBinaryData.length; i++) {
    exports.largeBinaryData[i] = i % 256;
}
