"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const fixtures = require("./fixtures");
const constants = require("../constants");
const __1 = require("..");
describe('A default ReadableStreamBuffer', function () {
    beforeEach(function () {
        this.buffer = new __1.ReadableStreamBuffer();
    });
    it('is a Stream', function () {
        chai_1.expect(this.buffer).to.be.an.instanceOf(require('stream').Stream);
    });
    it('is empty by default', function () {
        chai_1.expect(this.buffer.size()).to.equal(0);
    });
    it('has default backing buffer size', function () {
        chai_1.expect(this.buffer.maxSize()).to.equal(constants.DEFAULT_INITIAL_SIZE);
    });
    describe('when stopped', function () {
        beforeEach(function () {
            this.buffer.stop();
        });
        it('throws error on calling stop() again', function () {
            chai_1.expect(this.buffer.stop.bind(this.buffer)).to.throw(Error);
        });
        it('throws error on calls to put()', function () {
            chai_1.expect(this.buffer.put.bind(this.buffer)).to.throw(Error);
        });
    });
    it('emits end event when stopped', function (done) {
        this.buffer.on('end', done);
        this.buffer.stop();
        this.buffer.read();
    });
    it('emits end event after data, when stopped', function (done) {
        let str = '';
        this.buffer.on('readable', () => {
            str += (this.buffer.read() || Buffer.alloc(0)).toString('UTF8');
        });
        this.buffer.on('end', () => {
            chai_1.expect(str).to.equal(fixtures.unicodeString);
            done();
        });
        this.buffer.put(fixtures.unicodeString);
        this.buffer.stop();
    });
    it('pushes new data even if read when empty', function (done) {
        let str = '';
        this.buffer.on('readable', () => {
            str += (this.buffer.read() || Buffer.alloc(0)).toString('UTF8');
        });
        this.buffer.on('end', function () {
            chai_1.expect(str).to.equal(fixtures.unicodeString);
            done();
        });
        setTimeout(() => {
            this.buffer.put(fixtures.unicodeString);
            this.buffer.stop();
        }, constants.DEFAULT_FREQUENCY + 1);
    });
    describe('when writing binary data', function () {
        beforeEach(function (done) {
            this.buffer.put(fixtures.binaryData);
            this.buffer.once('readable', () => {
                this.data = this.buffer.read();
                done();
            });
        });
        it('results in a Buffer', function () {
            chai_1.expect(this.data).to.be.an.instanceOf(Buffer);
        });
        it('with the correct data', function () {
            chai_1.expect(this.data).to.deep.equal(fixtures.binaryData);
        });
    });
    it('supports putting in hex data', function (done) {
        this.buffer.put('BEEF', 'hex');
        this.buffer.once('readable', () => {
            var buf = this.buffer.read();
            chai_1.expect(buf[0]).to.equal(190);
            chai_1.expect(buf[1]).to.equal(239);
            done();
        });
    });
    describe('when writing binary data larger than initial backing buffer size', function () {
        beforeEach(function () {
            this.buffer.pause();
            this.buffer.put(fixtures.largeBinaryData);
        });
        it('buffer is correct size', function () {
            chai_1.expect(this.buffer.size()).to.equal(fixtures.largeBinaryData.length);
        });
        it('backing buffer is correct size', function () {
            chai_1.expect(this.buffer.maxSize()).to.equal(constants.DEFAULT_INITIAL_SIZE + constants.DEFAULT_INCREMENT_AMOUNT);
        });
    });
});
describe('A ReadableStreamBuffer using custom chunk size', function () {
    beforeEach(function (done) {
        this.buffer = new __1.ReadableStreamBuffer({
            chunkSize: 2
        });
        this.buffer.once('readable', () => {
            this.data = this.buffer.read();
            done();
        });
        this.buffer.put(fixtures.binaryData);
    });
    it('yields a Buffer with the correct data', function () {
        chai_1.expect(this.data).to.deep.equal(fixtures.binaryData.slice(0, 2));
    });
});
describe('A ReadableStreamBuffer using custom frequency', function () {
    beforeEach(function (done) {
        const startTime = new Date().getTime();
        this.buffer = new __1.ReadableStreamBuffer({
            frequency: 300
        });
        this.buffer.once('readable', () => {
            this.time = new Date().getTime() - startTime;
            done();
        });
        this.buffer.put(fixtures.binaryData);
    });
    it('gave us data after the correct amount of time', function () {
        // Wtfux: sometimes the timer is coming back a millisecond or two
        // faster. So we do a 'close-enough' assertion here ;)
        chai_1.expect(this.time).to.be.at.least(295);
    });
});
