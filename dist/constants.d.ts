export declare const DEFAULT_INITIAL_SIZE: number;
export declare const DEFAULT_INCREMENT_AMOUNT: number;
export declare const DEFAULT_FREQUENCY = 1;
export declare const DEFAULT_CHUNK_SIZE = 1024;
export declare const DEFAULT_ENCODING = "UTF8";
//# sourceMappingURL=constants.d.ts.map