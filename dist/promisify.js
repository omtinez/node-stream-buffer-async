"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const writable_1 = require("./writable");
function isReadable(stream) {
    return stream.pipe !== undefined && stream.unpipe !== undefined;
}
function isWritable(stream) {
    return stream.write !== undefined;
}
function isEndable(stream) {
    return stream.end !== undefined;
}
function StreamAsync(stream) {
    const stream_ = stream;
    if (isReadable(stream_)) {
        stream_.readAsync = (event = 'end') => {
            return new Promise((resolve, reject) => {
                // Setup error handling
                stream_.once('error', reject);
                // Pipe output into buffer
                const buffer = new writable_1.WritableStreamBuffer();
                stream_.pipe(buffer);
                // Terminate with output end
                stream_.once(event, () => {
                    stream_.unpipe(buffer);
                    resolve(buffer.getContents() || Buffer.from(''));
                });
            });
        };
    }
    if (isWritable(stream_)) {
        stream_.writeAsync = (chunk, encoding = 'UTF8') => {
            return new Promise((resolve, reject) => {
                stream_.once('error', reject);
                const result = stream_.write(chunk, encoding, err => err && reject(err));
                result ? resolve() : reject(new Error('Write operation failed'));
            });
        };
    }
    if (isEndable(stream_)) {
        stream_.endAsync = (chunk, encoding = 'UTF8') => {
            return new Promise((resolve, reject) => {
                stream_.once('error', reject);
                stream_.end(chunk, encoding, resolve);
            });
        };
    }
    return stream_;
}
exports.StreamAsync = StreamAsync;
