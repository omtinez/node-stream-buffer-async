/// <reference types="node" />
import * as stream from 'stream';
export interface ReadableStreamBufferOptions extends stream.ReadableOptions {
    frequency?: number;
    chunkSize?: number;
    initialSize?: number;
    incrementAmount?: number;
}
export declare class ReadableStreamBuffer extends stream.Readable {
    private size_;
    private stopped;
    private allowPush;
    private frequency;
    private chunkSize;
    private initialSize;
    private incrementAmount;
    private buffer;
    private timeout;
    constructor(opts?: ReadableStreamBufferOptions);
    size(): number;
    maxSize(): number;
    private increaseBufferIfNecessary;
    private sendData;
    stop(): void;
    private kickSendDataTask;
    put(data: any, encoding?: string): void;
    _read(): void;
}
//# sourceMappingURL=readable.d.ts.map