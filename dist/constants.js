"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DEFAULT_INITIAL_SIZE = (8 * 1024);
exports.DEFAULT_INCREMENT_AMOUNT = (8 * 1024);
exports.DEFAULT_FREQUENCY = 1;
exports.DEFAULT_CHUNK_SIZE = 1024;
exports.DEFAULT_ENCODING = 'UTF8';
