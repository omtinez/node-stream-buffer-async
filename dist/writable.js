"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const stream = require("stream");
const constants = require("./constants");
const _defaultOptions = {
    frequency: constants.DEFAULT_FREQUENCY,
    chunkSize: constants.DEFAULT_CHUNK_SIZE,
    initialSize: constants.DEFAULT_INITIAL_SIZE,
    incrementAmount: constants.DEFAULT_INCREMENT_AMOUNT
};
class WritableStreamBuffer extends stream.Writable {
    constructor(opts) {
        super(Object.assign({}, _defaultOptions, opts));
        this.size_ = 0;
        Object.assign(this, _defaultOptions, opts);
        this.buffer = Buffer.alloc(this.initialSize);
    }
    size() {
        return this.size_;
    }
    maxSize() {
        return this.buffer.length;
    }
    increaseBufferIfNecessary(incomingDataSize) {
        if ((this.buffer.length - this.size_) < incomingDataSize) {
            var factor = Math.ceil((incomingDataSize - (this.buffer.length - this.size_)) / this.incrementAmount);
            var newBuffer = Buffer.alloc(this.buffer.length + (this.incrementAmount * factor));
            this.buffer.copy(newBuffer, 0, 0, this.size_);
            this.buffer = newBuffer;
        }
    }
    getContents(length) {
        if (!this.size_)
            return Buffer.from('');
        const data = Buffer.alloc(Math.min(length || this.size_, this.size_));
        this.buffer.copy(data, 0, 0, data.length);
        if (data.length < this.size_)
            this.buffer.copy(this.buffer, 0, data.length);
        this.size_ -= data.length;
        return data;
    }
    getContentsAsString(encoding = constants.DEFAULT_ENCODING, length) {
        if (!this.size_)
            return '';
        var data = this.buffer.toString(encoding, 0, Math.min(length || this.size_, this.size_));
        var dataLength = Buffer.byteLength(data);
        if (dataLength < this.size_)
            this.buffer.copy(this.buffer, 0, dataLength);
        this.size_ -= dataLength;
        return data;
    }
    _write(chunk, encoding = constants.DEFAULT_ENCODING, callback) {
        this.increaseBufferIfNecessary(chunk.length);
        chunk.copy(this.buffer, this.size_, 0);
        this.size_ += chunk.length;
        callback();
    }
}
exports.WritableStreamBuffer = WritableStreamBuffer;
;
