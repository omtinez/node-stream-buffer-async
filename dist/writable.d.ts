/// <reference types="node" />
import * as stream from 'stream';
export interface WritableStreamBufferOptions extends stream.WritableOptions {
    initialSize?: number;
    incrementAmount?: number;
}
export declare class WritableStreamBuffer extends stream.Writable {
    private size_;
    private initialSize;
    private incrementAmount;
    private buffer;
    constructor(opts?: WritableStreamBufferOptions);
    size(): number;
    maxSize(): number;
    private increaseBufferIfNecessary;
    getContents(length?: number): Buffer;
    getContentsAsString(encoding?: string, length?: number): string;
    _write(chunk: any, encoding: string, callback: (error?: Error) => void): void;
}
//# sourceMappingURL=writable.d.ts.map